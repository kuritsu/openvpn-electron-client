# openvpn-electron-client

OpenVPN client with Electron GUI. This is an OpenVPN client which caches your credentials
and lets you specify a static challenge, usually a 2 factor authentication code with Google Authenticator
or Authy, then runs the openvpn client.

## Requirements

Requires the OpenVPN linux client. See [here](https://openvpn.net/index.php/access-server/section-faq-openvpn-as/client-configuration.html) for installation details.

## Structure

This Electron application needs these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `src/index.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `src/index.html` - A web page to render. This is the app's **renderer process**.
- `main.vue` - The framework for building the user interface is [Vue.js](https://vuejs.org/). This is the main app component.

You can learn more about each of these components within the [Quick Start Guide](https://electronjs.org/docs/tutorial/quick-start).

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://this-repo-url
# Go into the repository
cd openvpn-electron-client
# Install dependencies
npm install
# Run the app (you need sudo to run openvpn)
sudo npm start
```

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## To make/package

To make and package the project (for example, Linux RedHat and Debian-based packages,
notice that you need dpkg and rpm installed), execute:

```bash
npm run make
```

You can check the packages on the `out/make` directory.

## Configuration

By default, the app creates a `.openvpn-electron-client` dir in your home folder. Inside it you should
put a file named `client.ovpn` with your OpenVPN client configuration. **Beware:** your credentials will
be cached in a `credentials.txt` file inside this same dir, in plain text.

## License

MIT
